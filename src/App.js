import { Fragment, useLayoutEffect, useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Link, Switch } from 'react-router-dom';
import { DefaultLayout } from '~/components/Layout';
import { publicRoutes, privateRoutes } from '~/routes';
import Login from '~/pages/Login';

function App() {
  const login = localStorage.getItem('login');
  const [getToken, setGetToken] = useState('');
  useLayoutEffect(() => {
    setGetToken(localStorage.getItem('login'));
  }, []);

  return (
    // <Router>
    //   <Switch>
    //     <Route
    //       path="/login"
    //       render={() => {
    //         return !getToken ? <Login /> : <Redirect to="/" />;
    //       }}
    //     />
    //   </Switch>
    // </Router>
    <Router>
      <div className="App">
        <Routes>
          {privateRoutes.map((route, index) => {
            const Layout = route.layout === null ? Fragment : DefaultLayout;
            const Page = route.component;

            return (
              <Route
                key={index}
                path={route.path}
                element={
                  <Layout>
                    <Page />
                  </Layout>
                }
              />
            );
          })}
        </Routes>
      </div>
    </Router>
  );
}

export default App;
