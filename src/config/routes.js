const routes = {
  home: '/home',
  following: '/following',
  search: '/search',
  explore: '/explore',
  reels: '/reels',
  messages: '/messages',
  notification: '/notification',
  profile: '/profile',
  viewprofile: '/viewprofile',
  edit: '/edit',
  create: '/create',
  login: '/',
  register: '/register',
  change: '/change',
};

export default routes;
