import axios from 'axios';

const request = axios.create({
  baseURL: 'http://localhost:8080/api/',
});

const token = localStorage.getItem('token');

export const get = async (path, options = {}) => {
  const response = await request.get(path, options);
  return response.data;
};

export const post = async (path, options = {}) => {
  const response = await request.post(path, options);
  return response.data;
};

export const getPost = async () => {
  const res = await request
    .get(`/posts`, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      console.log(error);
    });
  return res;
};

export default request;
