import classNames from 'classnames/bind';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

import images from '~/assets/images';
import styles from './PostDetails.module.scss';
import Comment from '../comment';
import LikeButton from '~/components/LikeBtn/index';
import SaveButton from '~/components/saveBtn/index';
import CmtButton from '~/components/cmtBtn/index';
import ShareButton from '~/components/shareBtn/index';
import HomeHeader from '~/components/Layout/DefaultLayout/HomeLayout/Header';

const cx = classNames.bind(styles);
function PostDetails({ isLike, getPosts, getPost, avatar, photo, idPost, comment, username }) {
  const [comments, setComments] = useState('');
  const [post, setPost] = useState([]);
  const token = localStorage.getItem('token');

  function createComment() {
    if (comments !== '' && comments.length > 1) {
      axios
        .post(
          `http://localhost:8080/api/post/${idPost}/comment`,
          {
            content: comments,
          },
          {
            headers: {
              Authorization: 'Bearer ' + token,
            },
          },
        )
        .then((res) => {
          setComments('');
          getPost();

          setPost(getPost());
        })
        .catch((error) => {
          console.log('Error: ' + error);
        });
    }
  }

  return (
    <div className={cx('post-details')}>
      <div className={cx('content')}>
        <div className={cx('left-content')}>
          <img src={photo} alt="" />
        </div>
        <div className={cx('right')}>
          <HomeHeader headerAvatar={avatar} username={username} />
          <div className={cx('comments')}>
            {comment.total > '0'
              ? comment.list_comments.map((index) => {
                  return (
                    <Comment
                      avatar={index.users.avatar_url}
                      geta={getPosts}
                      key={index.id}
                      idCmt={index.id}
                      cmt={index.content}
                      username={index.users.username}
                      getPost={getPost}
                    />
                  );
                })
              : 'no Comment'}
          </div>
          <div className={cx('home-btn-icon')}>
            <div>
              {/* isLike={post.is_liked === true ? images.iconHeart : images.iconNotify} */}
              <LikeButton idpost={idPost} getPost={getPost} isLike={isLike === true ? images.iconHeart : images.iconNotify} />
              <CmtButton />
              <ShareButton />
            </div>
            <div>
              <SaveButton idpost={idPost} />
            </div>
          </div>
          <div className={cx('home-cmt')}>
            <div>
              <img src={images.iconSmile} alt="" />
              <input type="text" placeholder="Add a comment..." value={comments} onChange={(e) => setComments(e.target.value)} />
            </div>
            <button onClick={createComment}>Post</button>
          </div>
        </div>
      </div>
    </div>
  );
}
PostDetails.prototype = {
  avatar: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  idPost: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  like: PropTypes.string.isRequired,
  comment: PropTypes.array.isRequired,
  btnLike: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  close: PropTypes.string.isRequired,
};
export default PostDetails;
