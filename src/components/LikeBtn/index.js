import classNames from 'classnames/bind';
import { useState, useEffect } from 'react';
import axios from 'axios';

import images from '~/assets/images';
import styles from '~/components/compCss.module.scss';
import PropTypes from 'prop-types';
import * as request from '~/utils/requests';
const cx = classNames.bind(styles);

function LikeButton({ idpost, isLike, likea }) {
  const [likes, setLikes] = useState(isLike); //images.iconNotify
  // const [isLike, setIsLike] = useState(true); //isLike={post.is_like ? images.iconNotify : images.iconHeart}

  const token = localStorage.getItem('token');

  function likeF() {
    axios
      .get(`http://localhost:8080/api/post/like/${idpost}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        if (res.data.data.message === 'like successfully') {
          setLikes(images.iconHeart);
        } else {
          setLikes(images.iconNotify);
        }
        likea();
      })
      .catch((error) => {
        console.log('Error: ' + error);
      });
  }

  return (
    <div className={cx('btn')}>
      <button onClick={likeF}>
        <img src={likes} alt="Like" />
      </button>
    </div>
  );
}
LikeButton.prototype = {
  idpost: PropTypes.string.isRequired,
  isLike: PropTypes.string.isRequired,
};
export default LikeButton;
