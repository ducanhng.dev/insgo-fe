import classNames from 'classnames/bind';
import styles from './Message.css';
import images from '~/assets/images';

const cx = classNames.bind(styles);
function MessageCon({ own }) {
  return (
    <div className={own ? 'messageCon own' : 'messageCon'}>
      <div className="messageTop">
        <img className="messageImg" src={images.avatar1} />
        <p className="messageText">
          It's scary to change careers. I only gained confidence that I could code by working through the hundreds of hours of free lessons on
          freeCodeCamp. Within a year I had a six-figure job as a Software Engineer.
        </p>
      </div>
      <div className="messageBottom">1 hours ago</div>
    </div>
  );
}

export default MessageCon;
