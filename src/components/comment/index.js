import classNames from 'classnames/bind';
import { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './comment.module.scss';
import images from '~/assets/images';
import DelCmt from '~/components/DelCmt';
import axios from 'axios';
const cx = classNames.bind(styles);

function Comment({ avatar, geta, idCmt, cmt, username, getPost }) {
  const [close, setClose] = useState(false);
  const token = localStorage.getItem('token');
  const myUsername = localStorage.getItem('myUsername');
  function bntDel() {
    // if (username == myUsername) {
    axios
      .delete(`http://localhost:8080/api/post/comment/${idCmt}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setClose(false);
        getPost();
        geta();
      })
      .catch((error) => {
        console.log('Error: ' + error);
      });
    // }
  }
  function btnClose() {
    if (close === false) {
      setClose(true);
    } else {
      setClose(false);
    }
  }

  return (
    <div className={cx('comment')}>
      <div>
        <img src={avatar} alt="" />
      </div>
      <div>
        <div className={cx('username')}>
          {username} <span className={cx('cmt')}>{cmt}</span>{' '}
        </div>
        <div className={cx('btn-del')}>
          <button>Report</button>
          {username == myUsername && <button onClick={btnClose}>Delete</button>}
          {console.log(username)}
          {console.log(myUsername)}
          {close && <DelCmt idCmt={idCmt} />}
          {close && (
            <div className={cx('del-del')}>
              <button onClick={bntDel}>Delete</button>
            </div>
          )}
          {close && (
            <div className={cx('del-close')}>
              <button onClick={btnClose}>Cancel</button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
Comment.prototype = {
  cmt: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  idCmt: PropTypes.string.isRequired,
};

export default Comment;
