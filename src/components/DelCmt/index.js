import classNames from 'classnames/bind';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './Del.module.scss';
import axios from 'axios';

const cx = classNames.bind(styles);

function DelCmt({ idCmt }) {
  return (
    <div className={cx('del')}>
      <ul>
        <li className={cx('del-one')}>Delete comment?</li>
      </ul>
    </div>
  );
}
DelCmt.prototype = {
  idCmt: PropTypes.string.isRequired,
};

export default DelCmt;
