import { useState, useEffect } from 'react';
import classNames from 'classnames/bind';
import images from '~/assets/images';
import { useNavigate } from 'react-router-dom';
import * as request from '~/utils/requests';
import styles from '~/components/userPost/UserPost.module.scss';
import PropTypes from 'prop-types';
import axios from 'axios';

const cx = classNames.bind(styles);

function UserPosts({ postf, id, url, like, cmt }) {
  const token = localStorage.getItem('token');
  const navigate = useNavigate();

  const postUser = () => {
    request
      .get(`/user/posts`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const deleteF = () => {
    axios
      .delete(`http://localhost:8080/api/post/${id}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        // console.log(res.data.message);
        postf();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className={cx('user_post')}>
      <div className={cx('btn-del')}>
        <button
          onClick={() => {
            if (window.confirm('Are you sure you wish to delete this item?')) {
              deleteF();
            }
            // this.onCancel();
          }}
        >
          <img src={images.iconOptions} />
        </button>
      </div>
      <img src={url} alt="" />
      <div className={cx('like-cmt')}>
        <div>
          <div className={cx('icon')}>
            <img src={images.iconHeart} alt="" /> <span className={cx('text')}>{like}</span>
          </div>
          <div className={cx('icon')}>
            <img src={images.iconCmt} alt="" /> <span className={cx('text')}>{cmt}</span>
          </div>
        </div>
      </div>
      {/* </div> */}
    </div>
  );
}
UserPosts.prototype = {
  url: PropTypes.string.isRequired,
  like: PropTypes.string.isRequired,
  cmt: PropTypes.string.isRequired,
};
export default UserPosts;
