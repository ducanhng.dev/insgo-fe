import classNames from 'classnames/bind';
import styles from './Conversation.module.scss';
import images from '~/assets/images';
import PropTypes from 'prop-types';

const cx = classNames.bind(styles);

function Conversation({ username }) {
  return (
    <div className={cx('conversation')}>
      <img className={cx('conversation-img')} src={images.avatar1} alt="" />
      <span className={cx('conversation-name')}>{username}</span>
    </div>
  );
}
Conversation.prototype = {
  username: PropTypes.string.isRequired,
};
export default Conversation;
