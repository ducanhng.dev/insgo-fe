import { useState, useEffect } from 'react';
import classNames from 'classnames/bind';
import images from '~/assets/images';
import { useNavigate } from 'react-router-dom';
import * as request from '~/utils/requests';
import styles from '~/components/friendPost/friend.module.scss';
import PropTypes from 'prop-types';
import axios from 'axios';

const cx = classNames.bind(styles);

function FriendPost({ id, url, like, cmt }) {
  const token = localStorage.getItem('token');
  const navigate = useNavigate();

  const postUser = () => {
    request
      .get(`/user/posts`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const deleteF = () => {
    axios
      .delete(`http://localhost:8080/api/post/${id}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        // console.log(res.data.message);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className={cx('friend_post')}>
      <img src={url} alt="" />
      <div className={cx('like-cmt')}>
        <div>
          <div className={cx('icon')}>
            <img src={images.iconHeart} alt="" /> <span className={cx('text')}>{like}</span>
          </div>
          <div className={cx('icon')}>
            <img src={images.iconCmt} alt="" /> <span className={cx('text')}>{cmt}</span>
          </div>
        </div>
      </div>
      {/* </div> */}
    </div>
  );
}
FriendPost.prototype = {
  url: PropTypes.string.isRequired,
  like: PropTypes.string.isRequired,
  cmt: PropTypes.string.isRequired,
};
export default FriendPost;
