import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useState, useEffect, createContext } from 'react';
import images from '~/assets/images';
import styles from './HomeHeader.module.scss';

const cx = classNames.bind(styles);
export const UsernameContext = createContext();

function HomeHeader({ headerAvatar, username }) {
  const [name, setName] = useState(username);

  return (
    <UsernameContext.Provider value={name}>
      <div className={cx('home-header')}>
        <div>
          <img src={headerAvatar} alt="" className={cx('iconImg')} />
          <span>
            <Link
              to="/ViewProfile"
              onClick={() => {
                localStorage.setItem('username', username);
              }}
              style={{ color: 'white', textDecoration: 'none', fontSize: '1rem', fontWeight: '600' }}
            >
              {username}
            </Link>
          </span>
        </div>

        <div>
          <img src={images.iconOptions} alt="" className={cx('iconImg')} />
        </div>
      </div>
    </UsernameContext.Provider>
  );
}
HomeHeader.prototype = {
  headerAvatar: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
};
export default HomeHeader;
