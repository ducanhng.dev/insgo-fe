import classNames from 'classnames/bind';

import images from '~/assets/images';
import Button from '~/components/Button';
import styles from './Profile.module.scss';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import * as request from '~/utils/requests';

const cx = classNames.bind(styles);

function HomeProfile() {
  const [profile, setProfile] = useState([]);
  const [getUser, setGetUser] = useState([]);
  const [checkFl, setCheckFl] = useState(null);
  const [checkMe, setCheckMe] = useState(null);
  var items = getUser[Math.floor(Math.random() * getUser.length)];
  const token = localStorage.getItem('token');

  const profileMe = () => {
    request
      .get(`/user/me`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setProfile(res.data.profile);
        localStorage.setItem('avatar', res.data.avatar_url);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getListUsers = () => {
    request
      .get(`/user/list`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setGetUser(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    profileMe();
    getListUsers();

    console.log(profile.username);
  }, []);

  const getRandomUsers = (userList, count) => {
    const randomUsers = [];
    while (randomUsers.length < count) {
      const randomIndex = Math.floor(Math.random() * userList.length);
      if (randomIndex !== undefined) {
        const randomUser = userList.splice(randomIndex, 1)[0];
        randomUsers.push(randomUser);
      }
    }
    return randomUsers;
  };

  const randomUsers = getRandomUsers(getUser, 5);

  return (
    <div className={cx('home-profile')}>
      <div className={cx('profile-header')}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <img src={profile.avatar_url} alt="" className={cx('iconImg')} />
          <Button>
            <span className={cx('btn-username')}>
              {profile.username}
              {localStorage.setItem('myUsername', profile.username)}
            </span>
          </Button>
        </div>

        <div>
          <Button>Switch</Button>
        </div>
      </div>

      <div className={cx('header-fl')}>
        <label htmlFor="">Suggestions for you</label>
        <Button>See All</Button>
      </div>

      {randomUsers?.map((index, user) => {
        return (
          <div key={user} className={cx('fl-content')}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <img src={index?.avatar_url} alt="" className={cx('iconImg')} />
              <Link
                style={{ color: 'white' }}
                to="/ViewProfile"
                onClick={() => {
                  localStorage.setItem('username', index?.username);
                }}
              >
                <span className={cx('btn-username')}>{index?.username}</span>
              </Link>
            </div>
            <div>
              <Button>Follow</Button>
            </div>
          </div>
        );
      })}
      <div className={cx('footer')}>
        <div className={cx('footer-link-contact')}>
          <a href="#">About .</a>
          <a href="#"> Help .</a>
          <a href="#"> Press .</a>
          <a href="#"> API .</a>
          <a href="#"> Jobs .</a>
          <a href="#"> Privacy .</a>
          <a href="#"> Terms .</a>
          <a href="#"> Locations .</a>
          <a href="#"> Language .</a>
          <a href="#"> Meta Verifiled .</a>
          <a href="#"> Instagram Lite .</a>
        </div>
      </div>
    </div>
  );
}

export default HomeProfile;
