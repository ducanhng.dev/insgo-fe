import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

import images from '~/assets/images';
import styles from './HomeContent.module.scss';
import Button from '~/components/Button';
import LikeButton from '~/components/LikeBtn/index';
import SaveButton from '~/components/saveBtn/index';
import CmtButton from '~/components/cmtBtn/index';
import ShareButton from '~/components/shareBtn/index';
import HomeHeader from '~/components/Layout/DefaultLayout/HomeLayout/Header';
import PostDetails from '~/components/postDetails';
import * as request from '~/utils/requests';

const cx = classNames.bind(styles);

function HomeContent({ geta, avatar, idPost, caption, like, comment, username, photo }) {
  {
    /* các prop cần truyền 
    username, avatar, like, save, comment, number of like, times, captions */
  }
  const [comments, setComments] = useState('');
  const [post, setPost] = useState([]);
  const token = localStorage.getItem('token');
  const [close, setClose] = useState(false);
  const inputRef = useRef();
  const navigate = useNavigate();

  const getPostId = () => {
    request
      .get(`/post/${idPost}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setPost(res.data.post);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getPostId();
    localStorage.removeItem('username');
  }, []); //post

  function btnDisplay() {
    if (close === false) {
      setClose(true);
    } else {
      setClose(false);
    }
  }

  function createComment() {
    if (comments !== '' && comments.length > 1) {
      axios
        .post(
          `http://localhost:8080/api/post/${idPost}/comment`,
          {
            content: comments,
          },
          {
            headers: {
              Authorization: 'Bearer ' + token,
            },
          },
        )
        .then((res) => {
          setComments('');
          navigate('/home');
          geta();
          getPostId();
        })
        .catch((error) => {
          console.log('Error: ' + error);
        });
    }
  }
  return (
    <div>
      <HomeHeader headerAvatar={avatar} username={username} />
      <div className={cx('header-content')}>
        <div className={cx('images-home')}>
          <img src={photo} alt="Image user" />
        </div>
        <div className={cx('home-ct-padding')}>
          <div className={cx('home-btn-icon')}>
            <div>
              <LikeButton likea={geta} idpost={idPost} isLike={post.is_liked === true ? images.iconHeart : images.iconNotify} />
              <div style={{ height: '22px', padding: '8px' }}>
                <button onClick={() => inputRef.current.focus()}>
                  <img style={{ height: '24px' }} src={images.iconCmt} alt="Comment" />
                </button>
              </div>
              <ShareButton />
            </div>
            <div>
              <SaveButton idpost={idPost} />
            </div>
          </div>

          <div className={cx('like-number')}>
            <Button>{like}</Button> {/*  like */}
            <p>{caption}</p>
            <button onClick={btnDisplay}>{comment}</button>
            <div>
              {close && (
                <PostDetails
                  getPost={getPostId}
                  getPosts={geta}
                  idPost={post.id}
                  avatar={post.user.avatar_url}
                  photo={post.photo.url}
                  caption={post.caption}
                  like={post.likes.total}
                  username={post.user.username}
                  comment={post.comments}
                  isLike={post.isLike}
                />
              )}
              {close && (
                <button className={cx('btn-close')} onClick={btnDisplay}>
                  <img src={images.iconClose} alt="" />
                </button>
              )}
            </div>
            <br />
          </div>
          <div className={cx('home-cmt')}>
            <div>
              <img src={images.iconSmile} alt="" />
              <input ref={inputRef} type="text" placeholder="Add a comment..." value={comments} onChange={(e) => setComments(e.target.value)} />
            </div>
            <button onClick={createComment}>Post</button>
          </div>
        </div>
      </div>
    </div>
  );
}

HomeContent.prototype = {
  avatar: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  idPost: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  like: PropTypes.string.isRequired,
  comment: PropTypes.array.isRequired,
  btnLike: PropTypes.string.isRequired,
};
export default HomeContent;
