import React from 'react';
import classNames from 'classnames/bind';
import Tippy from '@tippyjs/react/headless';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useState, useEffect } from 'react';
import * as request from '~/utils/requests';
import { useNavigate } from 'react-router-dom';

import styles from './Sidebar.module.scss';
import images from '~/assets/images';
import config from '~/config';
import Menu, { MenuItem } from './Menu';

const cx = classNames.bind(styles);

function Sidebar() {
  const navigate = useNavigate();
  const [profile, setProfile] = useState([]);
  const token = localStorage.getItem('token');

  useEffect(() => {
    request
      .get(`/user/me`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setProfile(res.data.profile);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function logOut() {
    localStorage.clear();
    navigate('/');
  }

  return (
    <div style={{ width: '420px' }}>
      <Menu>
        <div style={{ paddingLeft: '20px' }}>
          <a href="/home">
            <h1>InsGo Media</h1>
          </a>
          <span className={cx('menu-span')}>
            <img src={images.iconHome} alt="" className={cx('iconImg')} />
            <MenuItem title="Home" to={config.routes.home} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconSearch} alt="" className={cx('iconImg')} />
            <MenuItem title="Search" to={config.routes.search} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconExplore} alt="" className={cx('iconImg')} />
            <MenuItem title="Chat GPT" to={config.routes.explore} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconReel} alt="" className={cx('iconImg')} />
            <MenuItem title="Reels" to={config.routes.reels} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconMessages} alt="" className={cx('iconImg')} />
            <MenuItem title="Messages" to={config.routes.messages} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconNotify} alt="" className={cx('iconImg')} />
            <MenuItem title="Notifications" to={config.routes.login} />
          </span>
          <span className={cx('menu-span')}>
            <img src={images.iconCreate} alt="" className={cx('iconImg')} />
            <MenuItem title="Create" to={config.routes.create} />
          </span>
          <span className={cx('menu-span')}>
            <img style={{ borderRadius: '50%' }} src={profile.avatar_url} alt="" className={cx('iconImg')} />
            <MenuItem title="Profile" to={config.routes.profile} />
          </span>
        </div>
        <div>
          <Tippy
            // visible
            interactive
            delay={[150, 200]}
            placement="bottom"
            render={(attrs) => (
              <ul className={cx('more-items')}>
                <li>
                  <MenuItem title="Setting" to={config.routes.edit} />
                </li>
                <li>activity</li>
                <li>Saved</li>
                <li>switch appearance</li>
                <li>Report a problem</li>
                <li>Switch accounts</li>
                <li>
                  <button style={{ color: 'white' }} onClick={logOut}>
                    Log out
                  </button>
                </li>
              </ul>
            )}
          >
            <span className={cx('menu-more')}>
              <img src={images.iconMenu} alt="" className={cx('iconImg')} />
              <span>More</span>
            </span>
          </Tippy>
        </div>
      </Menu>
    </div>
  );
}

export default Sidebar;
