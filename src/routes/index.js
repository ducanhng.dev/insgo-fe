import Home from '~/pages/Home';
import Following from '~/pages/Following';
import Search from '~/pages/Search';
import Messages from '~/pages/Messages';
import ChatGPT from '~/pages/ChatGPT';
import Login from '~/pages/Login';
import Register from '~/pages/Register';
import Profile from '~/pages/Profile';
import ViewProfile from '~/pages/ViewProfile';
import EditProfile from '~/pages/EditProfile';
import ChangePW from '~/pages/Change';
import Create from '~/pages/Create';
const privateRoutes = [
  { path: '/home', component: Home },
  { path: '/following', component: Following },
  { path: '/search', component: Search },
  { path: '/explore', component: ChatGPT },
  { path: '/messages', component: Messages },
  { path: '/profile', component: Profile },
  { path: '/viewProfile', component: ViewProfile },
  { path: '/edit', component: EditProfile },
  { path: '/change', component: ChangePW },
  { path: '/create', component: Create },
  { path: '/', component: Login, layout: null },
  { path: '/register', component: Register, layout: null },
];
const publicRoutes = [
  { path: '/login', component: Login, layout: null },
  { path: '/register', component: Register, layout: null },
];
export { publicRoutes, privateRoutes };
