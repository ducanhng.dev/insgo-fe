import classNames from 'classnames/bind';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';

import images from '~/assets/images';
import styles from '~/pages/Profile/Profile.module.scss';
import Footer from '../Footer';
import MenuTwo from '~/components/Layout/MenuTwo/index.js';
import * as request from '~/utils/requests';

const cx = classNames.bind(styles);

function EditProfile() {
  const [file, setFile] = useState(null);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [bio, setBio] = useState('');
  const [profile, setProfile] = useState('');
  const token = localStorage.getItem('token');
  const navigate = useNavigate();

  useEffect(() => {
    request
      .get(`/user/me`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => setProfile(res.data.profile))
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleFileUpload = (e) => {
    const fileImg = e.target.files[0];
    setFile(e.target.files[0]);
  };
  const handleUpdate = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('files', file);
    formData.append('first_name', firstName);
    formData.append('last_name', lastName);
    formData.append('address', address);
    formData.append('phone_number', phoneNumber);
    formData.append('bio', bio);
    // console.log(file);

    try {
      await axios.patch('http://localhost:8080/api/user/edit', formData, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      });
      navigate('/profile');
      console.log('update thanh cong');
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <div className={cx('edit-profile')}>
        <div className={cx('menu')}>
          <MenuTwo />
        </div>
        <div className={cx('edit-pro-right')}>
          <form onSubmit={handleUpdate}>
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <img src={profile.avatar_url == '' ? images.avatar1 : profile.avatar_url} alt="" />
              </div>
              <div>
                <label style={{ fontSize: '1rem', fontWeight: '500' }} htmlFor="">
                  {profile.username}
                </label>
                <br />
                <button>
                  Change profile photo
                  <input style={{ border: 'none' }} type="file" onChange={handleFileUpload} />
                </button>
              </div>
            </div>
            <br />

            {/* First name */}
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <label htmlFor="">First name</label>
              </div>
              <div className={cx('right')}>
                <input type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                <br />
                <div className={cx('font-small')}>
                  <label htmlFor="">
                    In most cases, you'll be able to change your username back to ly_anzy for another 14 days. <button>Learn more</button>
                  </label>
                </div>
              </div>
            </div>
            {/* Last name */}
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <label htmlFor="">Last name</label>
              </div>
              <div className={cx('right')}>
                <input type="text" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                <br />
                <div className={cx('font-small')}>
                  <label htmlFor="">
                    Editing your links is only available on mobile. Visit the Instagram app and edit your profile to change the websites in your bio.{' '}
                  </label>
                </div>
              </div>
            </div>

            {/* Infomations */}
            <div className={cx('content')}>
              <div className={cx('font-big')}></div>
              <div className={cx('right')}>
                <br />
                <div className={cx('font-small')}>
                  <p>Personal information</p>
                  <label htmlFor="">
                    Provide your personal information, even if the account is used for a business, a pet or something else. This won't be a part of
                    your public profile.
                  </label>
                </div>
              </div>
            </div>

            {/* Address */}
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <label htmlFor="">Address</label>
              </div>
              <div className={cx('right')}>
                <input type="text" value={address} onChange={(e) => setAddress(e.target.value)} />
                <br />
              </div>
            </div>
            <br />

            {/* Phone number */}
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <label htmlFor="">Phone number</label>
              </div>
              <div className={cx('right')}>
                <input type="text" value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} />
                <br />
              </div>
            </div>
            <br />

            {/* Bio */}
            <div className={cx('content')}>
              <div className={cx('font-big')}>
                <label htmlFor="">Bio</label>
              </div>
              <div className={cx('right')}>
                <input type="text" value={bio} onChange={(e) => setBio(e.target.value)} />
                <br />
              </div>
            </div>

            {/* Button submit */}
            <div className={cx('content')}>
              <div className={cx('font-big')}></div>
              <div className={cx('right')}>
                <br />
                <button type="submit">Submit</button>
                <button>Temporarily deactivate account</button>
                <br />
              </div>
            </div>
          </form>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default EditProfile;
