import classNames from 'classnames/bind';
import { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import images from '~/assets/images';
import styles from './ChatGPT.module.scss';
import * as request from '~/utils/requests';

const cx = classNames.bind(styles);

function ChatGPT() {
  const token = localStorage.getItem('token');
  const [chatGpt, setGPT] = useState('');
  const [jobs, setJobs] = useState([]);
  const inputRef = useRef();
  const [keyGPT, setKey] = useState('');

  function searchGPT() {
    axios
      // c2stSnFVc1JPUmVZNnJvcXdkTTZvN1FUM0JsYmtGSjM3VERNQ2dFUEoxc1pvV0ZBeUdP
      .post(
        `http://localhost:8080/api/user/gpt?token=c2stSnFVc1JPUmVZNnJvcXdkTTZvN1FUM0JsYmtGSjM3VERNQ2dFUEoxc1pvV0ZBeUdP`,
        {
          query: chatGpt,
        },
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((res) => {
        setJobs((prev) => [...prev, chatGpt]);
        setJobs((prev) => [...prev, res.data.data.message]);
        setGPT('');
        console.log(res.data.data.message);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  return (
    <div className={cx('chatgpt')}>
      <h1>ChatGPT</h1>
      {/* <div>
        {checkKeys == '' && (
          <div className={cx('check-key')}>
            <input type="text" placeholder="Send a key..." value={keyGPT} onChange={(e) => setKey(e.target.value)} />
            <button
              onClick={() => {
                localStorage.setItem('keyGPT', keyGPT);
              }}
            >
              submit
            </button>
          </div>
        )}
      </div> */}
      <div>
        <input ref={inputRef} type="text" placeholder="Send a message..." value={chatGpt} onChange={(e) => setGPT(e.target.value)} />
        <button
          className={cx('btn-clear')}
          onClick={() => {
            setGPT('');
            inputRef.current.focus();
          }}
        >
          {!!chatGpt && <img src={images.iconCloseB} alt="Like" />}
        </button>
        <button onClick={searchGPT} className={cx('btn-search')}>
          Submit
        </button>
      </div>
      <div className={cx('data-gpt')}>
        <ul>
          {jobs.map((job, index) => (
            <li key={index}>{job}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default ChatGPT;
