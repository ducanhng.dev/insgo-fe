import classNames from 'classnames/bind';
import { useState, useEffect, useContext } from 'react';
import * as request from '~/utils/requests';
import { UsernameContext } from '~/components/Layout/DefaultLayout/HomeLayout/Header';
import UserPosts from '~/components/userPost';
import FriendPost from '~/components/friendPost';
import images from '~/assets/images';
import Button from '~/components/Button';
import styles from './ViewProfile.module.scss';
import config from '~/config';
import Menu, { MenuItem } from '~/components/Layout/Sidebar/Menu';

const cx = classNames.bind(styles);

function ViewProfile() {
  const [userPosts, setUserPosts] = useState([]);
  const [profile, setProfile] = useState([]);
  const token = localStorage.getItem('token');
  const [checkFl, setCheckFl] = useState(null);
  const [checkMe, setCheckMe] = useState(null);
  const getUsername = localStorage.getItem('username');

  const usernameCt = useContext(UsernameContext);
  const follow = () => {
    request
      .get(`/user/${getUsername}/follow`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setCheckFl(res.data.message);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const unFollow = () => {
    request
      .get(`/user/${getUsername}/unfollow`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setCheckFl(res.data.message);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const checkFlF = () => {
    if (checkFl === 'follow successfully') {
      unFollow();
    } else {
      follow();
    }
    profileView();
  };

  const checkProMe = () => {
    request
      .get(`/user/me`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => setCheckMe(res.data.profile.username))
      .catch((error) => {
        console.log(error);
      });
  };
  const profileView = () => {
    request
      .get(`/user/${getUsername}`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setUserPosts(res.data.posts);
        setProfile(res.data.profile);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    checkProMe();
    profileView();
    follow();
    unFollow();
  }, []);

  return (
    <div className={cx('view-profile')}>
      <div className={cx('header-pro')}>
        <div className={cx('avatar-pro')}>
          <img style={{ width: '150px' }} src={profile.avatar_url} alt="" />
        </div>
        <div className={cx('header-right')}>
          <div>
            <div style={{ fontWeight: '600', fontSize: '1.2rem', marginRight: '20px' }}>{profile.username}</div>
            <div>
              <MenuItem to={config.routes.messages} title="Message" />
              {getUsername !== checkMe ? (
                <button className={cx('follow')} onClick={checkFlF}>
                  {checkFl === 'follow successfully' ? 'Follow' : 'Unfollow'}
                </button>
              ) : null}
              <img src={images.iconSetting} alt="" />
            </div>
          </div>
          <div>
            <div>{profile.number_of_posts} posts</div>
            <div>{profile.number_of_followings} followers</div>
            <div>{profile.number_of_followers} following</div>
          </div>
          <div className={cx('pro-name')}>{profile.bio}</div>
        </div>
      </div>
      <div className={cx('pro-menu')}>
        <div className={cx('pro-menu-post')}>
          <button>POSTS</button>
          <Button>REELS</Button>
          <button>SAVED</button>
          <Button>TAGGED</Button>
        </div>

        <div>
          {userPosts.map((index) => {
            return <FriendPost key={index.id} url={index.photo.url} like={index.likes.total} cmt={index.comments.total} />;
          })}
        </div>
      </div>
    </div>
  );
}

export default ViewProfile;
