import classNames from 'classnames/bind';
import images from '~/assets/images';
import Button from '~/components/Button';
import styles from './Messages.module.scss';
import Conversation from '~/components/Conversation';
import MessageCon from '~/components/Message';
import * as request from '~/utils/requests';
import { useContext, useEffect, useState, useRef } from 'react';
import axios from 'axios';
import io, { Socket } from 'socket.io-client';

const cx = classNames.bind(styles);

function Messages() {
  // const connSocket = io('localhost:8080/');
  const [conversations, setConversations] = useState([]);
  const [currentChat, setCurrentChat] = useState(null);
  const [messages, setMessages] = useState([]);
  const [username, setUsername] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [takeMessage, setTakeMessage] = useState(null);

  // useEffect(() =>{
  //   const socket = useRef(io('localhost:8080/'));
  //   socket.on('message1' (data) =>{
  //     setTakeMessage(
  //       sender_id: data.sender_id,

  //     )
  //   })
  // })

  const token = localStorage.getItem('token');

  const handleMessage = () => {
    setNewMessage('');
  };

  useEffect(() => {
    request
      .get(`/user/messages`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setUsername(res.data.chats);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div className={cx('chat')}>
      <div className={cx('chat-left')}>
        <div className={cx('left-header')}>Lý An Vy</div>
        <div className={cx('left-username')}>
          <div className={cx('username-header')}>
            {username?.map((index) => {
              // <div onClick={() => setCurrentChat('a')}>
              <Conversation key={index.id} username={index.receiver.username} />;
              // </div>;
            })}
          </div>
        </div>
      </div>
      <div className={cx('chat-right')}>
        <div className={cx('right-header')}>
          <div className={cx('header-user')}>
            <img src={images.avatar1} alt="" />
            <span>Le Trung Hieu</span>
          </div>
          <div className={cx('header-icon')}>
            <img src={images.iconCall} alt="" />
            <img src={images.iconVideoCall} alt="" />
            <img src={images.iconInfo} alt="" />
          </div>
        </div>

        <div className={cx('chatBox')}>
          <div className={cx('chatBoxWrapper')}>
            {currentChat ? (
              <>
                <div className={cx('chatBoxTop')}>
                  <MessageCon />
                  <MessageCon own={true} />
                  <MessageCon />
                </div>
                <div className={cx('chatBoxBottom')}>
                  <textarea
                    className={cx('chatMessage')}
                    value={newMessage}
                    onChange={(e) => {
                      setNewMessage(e.target.value);
                    }}
                    placeholder="Message..."
                  ></textarea>
                  <button className={cx('chatSubmitBtn')} onClick={handleMessage}>
                    Send
                  </button>
                </div>
              </>
            ) : (
              <span className={cx('noConversationText')}>Open a conversation to start a chat.</span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Messages;
