import classNames from 'classnames/bind';
import { useState, useEffect } from 'react';
import * as request from '~/utils/requests';

import UserPosts from '~/components/userPost';
import FriendPost from '~/components/friendPost';
import images from '~/assets/images';
import Button from '~/components/Button';
import styles from './Profile.module.scss';
import config from '~/config';
import Menu, { MenuItem } from '~/components/Layout/Sidebar/Menu';

const cx = classNames.bind(styles);

function Profile() {
  const [userPosts, setUserPosts] = useState([]);
  const [savePost, setSavePost] = useState([]);
  const [checkPost, setCheckPost] = useState(true);
  const [profile, setProfile] = useState('');
  const token = localStorage.getItem('token');

  function postSave() {
    request
      .get(`/post/save`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => setSavePost(res.data.posts), setCheckPost(false))
      .catch((error) => {
        console.log(error);
      });
  }
  const getUserProfile = () => {
    request
      .get(`/user/me`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => setProfile(res.data.profile))
      .catch((error) => {
        console.log(error);
      });
  };

  const postUser = () => {
    request
      .get(`/user/posts`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setUserPosts(res.data.posts);
        setCheckPost(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    request
      .get(`/user/posts`, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        setUserPosts(res.data.posts);
      })
      .catch((error) => {
        console.log(error);
      });
    getUserProfile();
  }, []);

  return (
    <div className={cx('profile')}>
      <div className={cx('header-pro')}>
        <div className={cx('avatar-pro')}>
          <img src={profile.avatar_url == '' ? images.avatar1 : profile.avatar_url} alt="" />
        </div>
        <div className={cx('header-right')}>
          <div>
            <div style={{ fontWeight: '600', fontSize: '1.2rem', marginRight: '20px' }}>{profile.username}</div>
            <div>
              <MenuItem to={config.routes.edit} title="Edit profile" />
              <img src={images.iconSetting} alt="" />
            </div>
          </div>
          <div>
            <div>{profile.number_of_posts} posts</div>
            <div>{profile.number_of_followers} followers</div>
            <div>{profile.number_of_followings} following</div>
          </div>
          <div className={cx('pro-name')}>{profile.bio}</div>
        </div>
      </div>
      <div className={cx('pro-menu')}>
        <div className={cx('pro-menu-post')}>
          <button onClick={postUser}>POSTS</button>
          <Button>REELS</Button>
          <button onClick={postSave}>SAVED</button>
          <Button>TAGGED</Button>
        </div>

        <div className={cx('imagePost')}>
          {checkPost &&
            userPosts.map((index) => {
              return (
                <UserPosts postf={postUser} id={index.id} key={index.id} url={index.photo.url} like={index.likes.total} cmt={index.comments.total} />
              );
            })}
          {!checkPost &&
            savePost.map((index) => {
              return <FriendPost id={index.id} key={index.id} url={index.photo.url} like={index.likes.total} cmt={index.comments.total} />;
            })}
        </div>
      </div>
    </div>
  );
}

export default Profile;
