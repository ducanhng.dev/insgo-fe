import React from 'react';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
// import lazyLoad from 'react-lazyload';

import classNames from 'classnames/bind';
import styles from './Create.module.scss';
import images from '~/assets/images';

const cx = classNames.bind(styles);

function Create() {
  const [file, setFile] = useState(null);
  const [captions, setCaptions] = useState('');
  const [avatar, setAvatar] = useState();
  const token = localStorage.getItem('token');
  const navigate = useNavigate();

  const handleFileUpload = (e) => {
    const fileImg = e.target.files[0];
    setFile(e.target.files[0]);
    fileImg.preview = URL.createObjectURL(fileImg);
    setAvatar(fileImg);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('files', file);
    formData.append('caption', captions);
    // console.log(file);

    try {
      await axios.post('http://localhost:8080/api/post', formData, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      });
      navigate('/home');
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className={cx('create')}>
      <div className={cx('create-header')}>Create new post</div>
      <br />
      <div className={cx('create-body')}>
        <img src={images.iconPostImg} alt="" />
        <br />
        <div>
          <br />
          <p>Drag photos and videos here</p>
          <br />
        </div>
        <form onSubmit={handleSubmit}>
          <input type="file" onChange={handleFileUpload} />
          <br />
          <br />
          {avatar && <img className={cx('avatar')} src={avatar.preview} />}
          <textarea placeholder="Write a caption" value={captions} onChange={(e) => setCaptions(e.target.value)} />
          <button type="submit">Post</button>
        </form>
      </div>
    </div>
  );
}
export default Create;
