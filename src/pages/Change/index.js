import classNames from 'classnames/bind';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

import images from '~/assets/images';
import Button from '~/components/Button';
import Menu, { MenuItem } from '~/components/Layout/Sidebar/Menu';
import config from '~/config';
import styles from '~/pages/Profile/Profile.module.scss';
import Footer from '../Footer';
import MenuTwo from '~/components/Layout/MenuTwo/index.js';

const cx = classNames.bind(styles);

function ChangePW() {
  const [newPw, setNewPw] = useState('');
  const [cfNewPw, setCfNewPw] = useState('');
  const token = localStorage.getItem('token');
  const navigate = useNavigate();
  const changPW = async () => {
    try {
      await axios.patch(
        'http://localhost:8080/api/auth/reset_password/ZHVjYW5uZy5kZXZAZ21haWwuY29t/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJpbnNnby1zZXJ2ZXIuand0Z28uaW8iLCJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2Nzk0MDgwMjYsImlzcyI6Imp3dGdvLmlvIiwidXNlciI6ImR1Y2FuaG5nLmRldiJ9.NMGuQnV4WLd94C1aq-odWf8CHIgfCjNgVnZnpnybItI',
        {
          data: {
            new_password: newPw,
            confirm_new_password: cfNewPw,
          },
        },
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      );
      navigate('/profile');
      console.log('update thanh cong');
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div>
      <div className={cx('edit-profile')}>
        <div className={cx('menu')}>
          <MenuTwo />
        </div>
        <div className={cx('edit-pro-right')}>
          <div className={cx('content')}>
            <div className={cx('font-big')}>
              <img src={images.avatar1} alt="" />
            </div>
            <div style={{ lineHeight: '50px' }}>
              <label htmlFor="">Ly_anzy</label>
            </div>
          </div>
          <br />
          {/* Old password */}
          <div className={cx('content')}>
            <div className={cx('font-big')}>
              <label htmlFor="">New password</label>
            </div>
            <div className={cx('right')}>
              <input type="password" value={newPw} onChange={(e) => setNewPw(e.target.value)} />
              <br />
            </div>
          </div>
          {/* New password */}
          <br />
          {/* Confirm password */}
          <div className={cx('content')}>
            <div className={cx('font-big')}>
              <label htmlFor="">Confirm new password</label>
            </div>
            <div className={cx('right')} style={{ paddingTop: '10px' }}>
              <input type="password" value={cfNewPw} onChange={(e) => setCfNewPw(e.target.value)} />
              <br />
            </div>
          </div>
          {/* Button submit */}
          <div className={cx('content')}>
            <div className={cx('font-big')}></div>
            <div className={cx('right')}>
              <br />
              <Button onclick={changPW}>Change password</Button>
              <button>Forgot password</button>
              <br />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default ChangePW;
